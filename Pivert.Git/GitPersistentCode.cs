﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pivert.Core;
using LibGit2Sharp;
using System.Diagnostics;
using System.Globalization;

namespace Pivert.Persistence.Git
{
    public class GitPersistentCode : IPersistentCode
    {
        internal GitPersistentCode(ObjectDatabase odb, Commit initialCommit, Action<Commit> whenNewCommit)
        {
            if (odb == null) { throw new ArgumentNullException("odb"); }
            if (initialCommit == null) { throw new ArgumentNullException("initialCommit"); }
            ObjectDatabase = odb;
            InitialCommit = initialCommit;
            LastCommit = InitialCommit;
            WhenNewCommit = whenNewCommit;
        }
        private Func<Signature> _comitterSignature = GitVersionnedCode.InternalSignature;
        public Func<Signature> ComitterSignature
        {
            get { return _comitterSignature; }
            set
            {
                if (value == null) { throw new ArgumentNullException(); }
                _comitterSignature = value;
            }
        }
        private Action<Commit> _whenNewCommit;
        public Action<Commit> WhenNewCommit
        {
            get { return _whenNewCommit; }
            set
            {
                if (value == null) { throw new ArgumentNullException(); }
                _whenNewCommit = value;
            }
        }
        private readonly ObjectDatabase ObjectDatabase;
        /// <summary>
        /// The commit at which the loading started. All non-yet loaded elements will be loaded from it.
        /// </summary>
        private readonly Commit InitialCommit;
        private Commit LastCommit;

        private Dictionary<Element, Tree> ElementsGitObject = new Dictionary<Element, Tree>();

        private Dictionary<Element, Guid> ElementsGuids = new Dictionary<Element, Guid>();

        private ISet<Element> ChangedElements = new HashSet<Element>();
        private void CheckIsValidList(Tree listTree, Exception onFail)
        {
            try
            {
                Func<string, int> toValidNumber = n =>
                {
                    if (n != "0" && n.StartsWith("0", StringComparison.InvariantCulture))
                    {
                        throw onFail;
                    }
                    int ret;
                    // The NumberStyles is specified to forbid -0.
                    if (!int.TryParse(n.Substring(1), System.Globalization.NumberStyles.None, CultureInfo.InvariantCulture, out ret))
                    {
                        throw onFail;
                    }
                    return ret;
                };
                var listElements =
                    (from e in listTree
                     where e.Name.StartsWith("_")
                     select toValidNumber(e.Name)).ToList();
                if (listElements.Count == 0) { return; }
                if (listElements.Max() != listElements.Count - 1) { throw onFail; }
                var indexesFound = new bool[listElements.Count];
                foreach (var element in listElements)
                {
                    if (element < 0) { throw onFail; }
                    Debug.Assert(!indexesFound[element]);
                    indexesFound[element] = true;
                }
                if (indexesFound.Any((entry) => !entry))
                {
                    throw onFail;
                }
            }
            catch (FormatException) { throw onFail; }
            catch (OverflowException) { throw onFail; }
        }
        /// <summary>
        /// Should be the only method that creates instances of Element.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private Element LoadShellElement(Tree from, ElementContainer parent)
        {
            Element ret;
            var elementTy = from.GetPivertType();
            if (elementTy == ElementType.List)
            {
                ret = new List(this, parent);
            }
            else if (elementTy == ElementType.Table)
            {
                ret = new Table(this, parent);
            }
            else if (elementTy == ElementType.Value)
            {
                ret = new Value(this, parent);
            }
            else if (elementTy == ElementType.Undefined)
            {
                ret = new Undefined(this, parent);
            }
            else
            {
                Debug.Fail("Paths are not a valid type while loading an element.");
                return null;
            }
            ElementsGitObject[ret] = from;
            var guid = from.GetGuid();
            if (guid != null)
            {
                ElementsGuids[ret] = (Guid)guid;
            }
            return ret;
        }
        private void LoadContainerContents(ElementContainer elementToLoad, ElementType type, Action<string, Element> onNewEntry)
        {
            foreach (var entry in ElementsGitObject[elementToLoad])
            {
                if (!entry.Name.StartsWith("_")) { continue; }
                var entryPos = entry.Name.Substring(1);
                onNewEntry(entryPos, LoadShellElement(entry.Target as Tree, elementToLoad));
            }
        }
        public void RegisterChangedElement(Element e)
        {
            ChangedElements.Add(e);
        }
        public Dictionary<string, Element> LoadTableContents(Table element)
        {
            var ret = new Dictionary<string, Element>();
            LoadContainerContents(element, ElementType.Table,
                (key, subElement) => { ret[key] = subElement; });
            return ret;
        }
        public List<Element> LoadListContents(List element)
        {
            var ret = new List<Element>();
            LoadContainerContents(element, ElementType.List,
                (key, subElement) => { ret.Insert(int.Parse(key), subElement); });
            return ret;
        }

        public string LoadValueContents(Value element)
        {
            var valueTreeEntry = ElementsGitObject[element];
            Debug.Assert(valueTreeEntry.GetPivertType() == ElementType.Value);
            return (valueTreeEntry[GitTreeKeys.Value].Target as Blob).ContentAsText();
        }

        private Element _topLevel;
        public Element TopLevel
        {
            get
            {
                if (_topLevel == null)
                {
                    _topLevel = LoadShellElement(InitialCommit.Tree, null);
                }
                return _topLevel;
            }
            set
            {
                if (value == null)
                {
                    value = new Undefined(this, null);
                }
                if (value.Parent != null)
                {
                    value.RemoveParent();
                }
                _topLevel = value;
                value.SetCodeForTopLevel(this);
                RegisterChangedElement(value);
            }
        }

        private ISet<Element> GetChangedElementsAccordingToGitSinceLastCommit()
        {
            var modifiedElements = new HashSet<Element>(ChangedElements);
            var elementsToAnalyze = new Stack<Element>(modifiedElements);
            while (elementsToAnalyze.Count != 0)
            {
                var parent = elementsToAnalyze.Pop().Parent;

                if (parent != null && !modifiedElements.Contains(parent))
                {
                    modifiedElements.Add(parent);
                    elementsToAnalyze.Push(parent);
                }
            }
            var modifiedElementsReachableFromTop = new HashSet<Element>();
            elementsToAnalyze = new Stack<Element>();
            if (!modifiedElements.Contains(TopLevel))
            {
                elementsToAnalyze.Push(TopLevel);
            }
            while (elementsToAnalyze.Count != 0)
            {
                var toAnalyse = elementsToAnalyze.Pop();
                modifiedElementsReachableFromTop.Add(toAnalyse);
                if (!(toAnalyse is ElementContainer))
                {
                    continue;
                }
                foreach (var element in toAnalyse as ElementContainer)
                {
                    if (modifiedElements.Contains(element))
                    {
                        elementsToAnalyze.Push(element);
                    }
                }
            }
            return modifiedElementsReachableFromTop;
        }
        private void InvalidateElementsGitId(IEnumerable<Element> toInvalidate)
        {
            foreach (var changedElement in toInvalidate)
            {
                ElementsGitObject.Remove(changedElement);
            }
        }
        private Guid? GetElementId(Element element)
        {
            // TODO: Implement Ids.
            return null;
        }
        private void AddTypeToTreeDef(TreeDefinition treeDef, string type)
        {
            treeDef.Add(GitTreeKeys.Type, GitVersionnedCode.CreateBlobBlobFromString(ObjectDatabase, type), Mode.NonExecutableFile);
        }
        private Tree GetElementTree(Element element)
        {
            if (ElementsGitObject.ContainsKey(element))
            {
                return ElementsGitObject[element];
            }
            var treeDef = new TreeDefinition();
            if (element is Value)
            {
                var valueInBlob = GitVersionnedCode.CreateBlobBlobFromString(ObjectDatabase, (element as Value).Content);
                AddTypeToTreeDef(treeDef, TreeType.Value);
                treeDef.Add(GitTreeKeys.Value, GitVersionnedCode.CreateBlobBlobFromString(ObjectDatabase, (element as Value).Content), Mode.NonExecutableFile);
            }
            else if (element is Undefined)
            {
                AddTypeToTreeDef(treeDef, TreeType.Undefined);
            }
            else if (element is Table)
            {
                AddTypeToTreeDef(treeDef, TreeType.Table);
                foreach (var kv in element as IDictionary<string, Element>)
                {
                    if (kv.Key.Contains('/')) { throw new NotImplementedException("Sorry, but serialization of the keys containing a '/' is not implemented yet."); }
                    treeDef.Add("_"+kv.Key, GetElementTree(kv.Value));
                }
            }
            else if (element is List)
            {
                AddTypeToTreeDef(treeDef, TreeType.List);
                var i = 0;
                foreach (var subElement in element as List)
                {
                    treeDef.Add("_" + i.ToString(), GetElementTree(subElement));
                    ++i;
                }
            }
            else
            {
                throw new ArgumentOutOfRangeException(
                    "element",
                    "Don't know how to serialize " + element.GetType().FullName + " .");
            }
            return ObjectDatabase.CreateTree(treeDef);
        }
        public void Commit(string data)
        {
            InvalidateElementsGitId(GetChangedElementsAccordingToGitSinceLastCommit());
            var commit = ObjectDatabase.CreateCommit(
                data,
                ComitterSignature(), ComitterSignature(),
                GetElementTree(TopLevel), new Commit[] { LastCommit });
            WhenNewCommit(commit);
        }
    }
}
