﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using System.Diagnostics;

namespace Pivert.Persistence.Git
{
    internal enum ElementType
    {
        Undefined,
        Value,
        List,
        Table,
    }
    internal static class GitTreeKeys
    {
        public const string Type = "type";
        public const string Guid = "guid";
        public const string Value = "value";
    }
    internal static class TreeType
    {
        public const string Table = "table";
        public const string List = "list";
        public const string Value = "value";
        public const string Undefined = "undefined";
    }
    internal static class GitTreeExtensionMethods
    {
        public static ElementType GetPivertType(this Tree @this)
        {
            var ty = (@this[GitTreeKeys.Type].Target as Blob).ContentAsText();
            if (ty == TreeType.Table)
            {
                return ElementType.Table;
            }
            if (ty == TreeType.List)
            {
                return ElementType.List;
            }
            if (ty == TreeType.Value)
            {
                return ElementType.Value;
            }
            if (ty == TreeType.Undefined)
            {
                return ElementType.Undefined;
            }
            var thrown = new MalformatedRepositoryException("Tree type unknown.");
            thrown.Data["type"] = ty;
            throw thrown;
        }
        public static Guid? GetGuid(this Tree @this)
        {
            var guidEntry = @this[GitTreeKeys.Guid];
            if (guidEntry == null)
            {
                return null;
            }
            else
            {
                return Guid.Parse((guidEntry.Target as Blob).ContentAsText());
            }
        }
    }
}
