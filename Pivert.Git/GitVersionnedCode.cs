﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pivert.Core;
using LibGit2Sharp;
using System.Diagnostics;
using System.Globalization;

namespace Pivert.Persistence.Git
{
    public class GitVersionnedCode : IVersionnedCode, IDisposable
    {
        public static Signature InternalSignature()
        {
            return new Signature("internal", "none", DateTimeOffset.Now);
        }
        public static Blob CreateBlobBlobFromString(ObjectDatabase odb, string str)
        {
            var byteStream = new System.IO.MemoryStream(UTF8Encoding.UTF8.GetBytes(str));
            return odb.CreateBlob(byteStream);
        }
        public static void InitNew(string path)
        {
            Repository.Init(path, isBare:true);
            using (var r = new Repository(path))
            {
                var odb = r.ObjectDatabase;
                var refToTopLevel = CreateBlobBlobFromString(odb, TreeType.Undefined);
                var rootDef = new TreeDefinition();
                rootDef.Add(GitTreeKeys.Type, CreateBlobBlobFromString(odb, TreeType.Undefined), Mode.NonExecutableFile);
                var root = odb.CreateTree(rootDef);
                var commit = odb.CreateCommit("Initial commit.", InternalSignature(), InternalSignature(), root, new Commit[0]);
                r.Branches.Add("master", commit, true);
            }
        }
        private Repository Repository;
        public GitVersionnedCode(string path)
        {
            // TODO Add a flag to throw an exception instead of creating.
            if (!LibGit2Sharp.Repository.IsValid(path))
            {
                InitNew(path);
            }
            Repository = new Repository(path);
        }
        public void Dispose()
        {
            Repository.Dispose();
        }
        public GitPersistentCode Checkout(string branchName, string newBranchName = null)
        {
            if (newBranchName == null) { newBranchName = branchName; }
            var branch = Repository.Branches.First(b => b.Name == branchName);
            return new GitPersistentCode(Repository.ObjectDatabase, branch.Tip, (c) => Repository.Branches.Add(newBranchName, c, true));
        }
        public IList<string> Branches
        {
            get { return Repository.Branches.Select(b=>b.Name).ToList(); }
        }

        public string LatestState
        {
            get { throw new NotImplementedException(); }
        }

        public string CurrentBranchId
        {
            get { throw new NotImplementedException(); }
        }

        public string LatestChangeSetID
        {
            get { throw new NotImplementedException(); }
        }

        public string CurrentChangeSetID
        {
            get { throw new NotImplementedException(); }
        }

        public void ToChangeSet(string targetChangeset)
        {
            throw new NotImplementedException();
        }

        public string Branch()
        {
            throw new NotImplementedException();
        }
    }
}
