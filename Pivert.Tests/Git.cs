﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Pivert.Persistence.Git;
using Pivert.Core;

namespace Pivert.Core.Tests
{
    [TestClass]
    public class Git
    {
        const string GitReposLocation = "./testgit";
        GitVersionnedCode versionnedCode;
        [TestInitialize]
        public void SetUp()
        {
            var dir = Directory.CreateDirectory(GitReposLocation);
            versionnedCode = new GitVersionnedCode(GitReposLocation);
            FillInitialRepos();
        }
        /// <summary>
        /// From http://stackoverflow.com/a/3867306/859757 .
        /// Because git objects are readonly, Delete would choke on them.
        /// </summary>
        /// <param name="parentDirectory">The origin directory of the cleanup.</param>
        private void ClearReadOnly(DirectoryInfo parentDirectory)
        {
            if (parentDirectory != null)
            {
                parentDirectory.Attributes = FileAttributes.Normal;
                foreach (FileInfo fi in parentDirectory.GetFiles())
                {
                    fi.Attributes = FileAttributes.Normal;
                }
                foreach (DirectoryInfo di in parentDirectory.GetDirectories())
                {
                    ClearReadOnly(di);
                }
            }
        }
        [TestCleanup]
        public void TearDown()
        {
            versionnedCode.Dispose();
            ClearReadOnly(new DirectoryInfo(GitReposLocation));
            Directory.Delete(GitReposLocation, recursive: true);
        }
        public void LoadInitialRepos()
        {
            var code = versionnedCode.Checkout("master", "LoadInitialRepos");
            Assert.IsTrue(code.TopLevel is Undefined, "Initial TopLevel element should be Undefined.");
        }
        public void FillInitialRepos()
        {
            // Branch1Name should look like this:
            // {"val":[undefined,"someVal"],"type":"someType"}
            var code = versionnedCode.Checkout("master", Branch1Name);
            var table = new Table();
            var list = new List();
            list.Add(new Undefined());
            table["val"] = list;
            list.Add(new Value("someVal"));
            code.TopLevel = table;
            table["type"] = new Value("someType");
            code.Commit("This is a nice commit.");
        }
        const string Branch1Name = "SaveAndLoadVariousElements";
        [TestMethod]
        public void LoadVariousElements()
        {
            var loadedCode = versionnedCode.Checkout(Branch1Name);
            var table = (Table)loadedCode.TopLevel;
            Assert.IsTrue(((Value)table["type"]).Content == "someType");
            var list = (List)table["val"];
            Assert.IsTrue(((Value)list[1]).Content == "someVal");
            Assert.IsTrue(list[0] is Undefined);
            LoadInitialRepos();
        }
        [TestMethod]
        public void MoveElementsFromCodeToCode()
        {
            var branch2Name = "branch2";
            {
                var loadedCode = versionnedCode.Checkout(Branch1Name);
                var newCode = versionnedCode.Checkout(Branch1Name, branch2Name);
                var list = new List();
                list.Add((loadedCode.TopLevel as Table)["val"]);
                Assert.IsFalse((loadedCode.TopLevel as Table).ContainsKey("val"));
                newCode.TopLevel = list;
                newCode.Commit("Nice commit.");
            }
            {
                var newCode = versionnedCode.Checkout(branch2Name);
                var subList = ((newCode.TopLevel as List)[0] as List);
                Assert.IsTrue(subList[0] is Undefined);
                Assert.IsTrue((subList[1] as Value).Content=="someVal");
            }
        }
    }
}
