﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pivert.Core;
using System.Linq;
using System.Dynamic;

namespace Pivert.Core.Tests
{
    [TestClass]
    public class UnitTest1
    {
        private static string testString1 = "hi";
        private static string testString2 = "hoy";
        private Table table;
        private List list;
        private Value value1;
        [TestInitialize]
        public void Init()
        {
            table = new Table();
            list = new List();
            value1 = new Value(testString1);
        }
        [TestMethod]
        public void SmokeTest1()
        {
            table.Add("k", new List(new Element[] { value1 }));
        }
        [TestMethod]
        public void Table_AddElement()
        {
            table["x"] = value1;
            Assert.AreEqual(table["x"], value1);
            Assert.AreEqual((string)table.ToDynamic().x, testString1);
            table.ToDynamic().y = testString2;
            Assert.AreEqual(table.ToDynamic()._wrapped["y"].Content, testString2);
        }
        [TestMethod]
        public void List_AddElement()
        {
            list.Add(value1);
            Assert.AreEqual(list.Count, 1);
            Assert.AreEqual(list[0], value1);
            Assert.AreEqual((string)list.ToDynamic()[0], testString1);
        }
    }
}
