#Introduction#

This project toys with the idea of using a single non-textual representation for code trough the whole pipeline.

##In source control##

It makes semantic merge easier, as the granularity is not at the line level but at the AST element level.

This functionality is provided by [pivert](https://bitbucket.org/clement_bellot/pivert).

##In the IDE##

Giving up text allows more powerful code visualizations, that can vary function of the user preferences and what he wants to see (debugging, editing, reading, optimizing, looking for a bug...), and that does not have to preserve the underlying text representation (for example, it doesn't have to care about whitespace/formatting).

#Status#

The source control is roughly working, the semantic merge is not yet implemented, and very little work was done on the IDE.